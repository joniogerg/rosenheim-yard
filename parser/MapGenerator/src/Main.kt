import java.io.File
import java.lang.Exception
import java.util.*
import java.util.concurrent.Executors
import kotlin.collections.ArrayList

fun main(args: Array<String>){

    val scanner = Scanner(System.`in`)

    println("Wie viele Knoten gibt es?")
    val size = scanner.nextInt()

    val data = arrayOfNulls<Array<Int?>>(size)

    for(i in 0 until size){
        data[i] = arrayOfNulls(size)

        print("Von Knotenpunkt ${i + 1} zu: ")
        val str = scanner.next().replace(" ", "")
        val operations = str.split(",")
        var wasError = false
        for(j in 0 until size){
            data[i]!![j] = 0
            try{
                if(str != "-"){
                    for(op in operations){
                        val splitOp = op.split(":")
                        if(j + 1 == splitOp[0].toInt()){
                            data[i]!![j] = splitOp[1].toInt()
                            break
                        }
                    }
                }
            }catch(ex: Exception){
                if(!wasError)
                    println("Fehlerhafte eingabe. Bitte nachträglich in Excel eintragen")
                wasError = true
            }
        }
    }

//    println(data.joinToString("\n") { it?.joinToString(", ") ?: "null" })

    val file = File(System.getProperty("user.dir"), "map.csv")
    while(true){
        try{
            if(file.exists()) file.delete()
            file.writeText(data.joinToString("\n") { it?.joinToString(";") ?: "null" })
            println("Datei gespeichert")
            break
        }catch(ex: Exception){
            println("Datei konnte nicht beschrieben werden, probiere in 2 Sekunden nochmal")
            Thread.sleep(2000)
        }
    }

}