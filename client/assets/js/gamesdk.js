(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["gamesdk"] = factory();
	else
		root["gamesdk"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/isomorphic-ws/browser.js":
/*!***********************************************!*\
  !*** ./node_modules/isomorphic-ws/browser.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {// https://github.com/maxogden/websocket-stream/blob/48dc3ddf943e5ada668c31ccd94e9186f02fafbd/ws-fallback.js

var ws = null

if (typeof WebSocket !== 'undefined') {
  ws = WebSocket
} else if (typeof MozWebSocket !== 'undefined') {
  ws = MozWebSocket
} else if (typeof global !== 'undefined') {
  ws = global.WebSocket || global.MozWebSocket
} else if (typeof window !== 'undefined') {
  ws = window.WebSocket || window.MozWebSocket
} else if (typeof self !== 'undefined') {
  ws = self.WebSocket || self.MozWebSocket
}

module.exports = ws

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ "./src/GameClient.ts":
/*!***************************!*\
  !*** ./src/GameClient.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const EventEmitter_1 = __webpack_require__(/*! ./event/EventEmitter */ "./src/event/EventEmitter.ts");
const Room_1 = __webpack_require__(/*! ./Room */ "./src/Room.ts");
const WebSocket = __webpack_require__(/*! isomorphic-ws */ "./node_modules/isomorphic-ws/browser.js");
class GameClient extends EventEmitter_1.EventEmitter {
    constructor(url, onConnected) {
        super();
        this.url = url;
        this.onConnected = onConnected;
        this.session_id = "";
        this.room = null;
        try {
            if (new Date(sessionStorage.getItem("sessionIdExpire")).getTime() < new Date().getTime()) {
                sessionStorage.removeItem("sessionId");
                sessionStorage.removeItem("sessionIdExpire");
            }
            this.session_id = sessionStorage.getItem("sessionId") ? sessionStorage.getItem("sessionId") : "";
        }
        catch (ev) { }
        this.socket = new WebSocket(url);
        this.setupSocket();
    }
    createRoom(configuration) {
        return new Promise(resolve => {
            this.once("room created", id => {
                console.debug("id: ", id);
                this.room = new Room_1.Room(id, configuration, this);
                resolve(this.room);
            });
            this.send("room create", configuration);
        });
    }
    joinRoom(id) {
        return new Promise(resolve => {
            const createRoom = (roomData, reconnected) => {
                console.debug("data", roomData);
                this.room = new Room_1.Room(roomData.id, roomData.configuration, this);
                resolve({ room: this.room, reconnected: reconnected });
            };
            this.once("room joined", data => { createRoom(data, false); });
            this.once("room reconnect", data => { createRoom(data, true); });
            this.once("room join error", error => { console.error("Error while joining room " + id, error); });
            this.send("room join", {
                id: id,
                data: {}
            });
        });
    }
    getRooms() {
        return new Promise(resolve => {
            this.once("room list", rooms => {
                console.log("Roooooms", rooms);
            });
            this.send("room list");
        });
    }
    setupSocket() {
        this.socket.onopen = ev => {
            this.send("register");
        };
        this.socket.onmessage = ev => {
            const message = JSON.parse(ev.data.toString());
            this.session_id = message.session_id;
            let data;
            try {
                data = JSON.parse(message.data);
            }
            catch (e) {
                data = message.data;
            }
            if (message.context === "ALL") {
                this.call(message.event, data);
            }
            else if (message.context === "ROOM") {
                if (this.room) {
                    this.room.call(message.event, data);
                }
            }
        };
        this.socket.onerror = err => {
            console.error(err);
        };
        this.socket.onclose = (ev) => {
            console.debug("Conenction closed", ev);
        };
        this.once("reconnect", (data) => {
            this.onConnected(true);
        });
        this.once("registered", sessionId => {
            // keep session id for 3 days
            sessionStorage.setItem("sessionId", sessionId);
            sessionStorage.setItem("sessionIdExpire", new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 3).toUTCString());
            this.session_id = sessionId;
            this.onConnected(false);
        });
        this.on("error", error => {
            console.error(error);
        });
    }
    send(event, data = null, context = "ALL") {
        let msg = data;
        if (data instanceof Object)
            msg = JSON.stringify(data);
        console.debug("sending event", event, data);
        // TODO: Check if socket is connected
        this.socket.send(JSON.stringify({
            event: event,
            session_id: this.session_id,
            context: context,
            data: msg
        }));
    }
}
exports.GameClient = GameClient;


/***/ }),

/***/ "./src/Room.ts":
/*!*********************!*\
  !*** ./src/Room.ts ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const EventEmitter_1 = __webpack_require__(/*! ./event/EventEmitter */ "./src/event/EventEmitter.ts");
class Room extends EventEmitter_1.EventEmitter {
    constructor(id, configuration, client) {
        super();
        this.id = id;
        this.configuration = configuration;
        this.client = client;
    }
    send(event, data) {
        this.client.send(event, data, "ROOM:" + this.id);
    }
}
exports.Room = Room;


/***/ }),

/***/ "./src/event/EventEmitter.ts":
/*!***********************************!*\
  !*** ./src/event/EventEmitter.ts ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class EventEmitter {
    constructor() {
        this.eventHandlers = {};
    }
    on(event, func) {
        if (!event || !func)
            return;
        if (!this.eventHandlers[event]) {
            this.eventHandlers[event] = [];
        }
        this.eventHandlers[event].push(func);
    }
    once(event, callback) {
        this.on("#once_" + event, callback);
    }
    call(event, data) {
        if (this.eventHandlers[event]) {
            this.eventHandlers[event].forEach(func => func(data));
        }
        if (this.eventHandlers["*"]) {
            this.eventHandlers["*"].forEach(func => func(data, event));
        }
        if (this.eventHandlers["#once_" + event]) {
            const funcs = this.eventHandlers["#once_" + event];
            for (let i = 0; i < funcs.length; i++) {
                funcs[i](data);
                this.eventHandlers["#once_" + event].splice(i, 1);
            }
        }
    }
    removeEventHandler(event, func) {
        if (this.eventHandlers[event]) {
            const index = this.eventHandlers[event].indexOf(f => f === func);
            if (index >= 0) {
                this.eventHandlers[event].splice(index, 1);
            }
        }
    }
    removeEventHandlers(event) {
        this.eventHandlers[event] = [];
    }
}
exports.EventEmitter = EventEmitter;


/***/ }),

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var GameClient_1 = __webpack_require__(/*! ./GameClient */ "./src/GameClient.ts");
exports.GameClient = GameClient_1.GameClient;
var Room_1 = __webpack_require__(/*! ./Room */ "./src/Room.ts");
exports.Room = Room_1.Room;
var EventEmitter_1 = __webpack_require__(/*! ./event/EventEmitter */ "./src/event/EventEmitter.ts");
exports.EventEmitter = EventEmitter_1.EventEmitter;


/***/ })

/******/ });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9nYW1lc2RrL3dlYnBhY2svdW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbiIsIndlYnBhY2s6Ly9nYW1lc2RrL3dlYnBhY2svYm9vdHN0cmFwIiwid2VicGFjazovL2dhbWVzZGsvLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy13cy9icm93c2VyLmpzIiwid2VicGFjazovL2dhbWVzZGsvKHdlYnBhY2spL2J1aWxkaW4vZ2xvYmFsLmpzIiwid2VicGFjazovL2dhbWVzZGsvLi9zcmMvR2FtZUNsaWVudC50cyIsIndlYnBhY2s6Ly9nYW1lc2RrLy4vc3JjL1Jvb20udHMiLCJ3ZWJwYWNrOi8vZ2FtZXNkay8uL3NyYy9ldmVudC9FdmVudEVtaXR0ZXIudHMiLCJ3ZWJwYWNrOi8vZ2FtZXNkay8uL3NyYy9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0QsTztRQ1ZBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7O0FDbEZBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQSxDQUFDO0FBQ0Q7QUFDQSxDQUFDO0FBQ0Q7QUFDQSxDQUFDO0FBQ0Q7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7OztBQ2hCQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLDRDQUE0Qzs7QUFFNUM7Ozs7Ozs7Ozs7Ozs7OztBQ25CQSxzR0FBb0Q7QUFDcEQsa0VBQThCO0FBQzlCLHNHQUEyQztBQUUzQyxNQUFhLFVBQVcsU0FBUSwyQkFBWTtJQU14QyxZQUFtQixHQUFXLEVBQVMsV0FBMkM7UUFDOUUsS0FBSyxFQUFFO1FBRFEsUUFBRyxHQUFILEdBQUcsQ0FBUTtRQUFTLGdCQUFXLEdBQVgsV0FBVyxDQUFnQztRQUpsRixlQUFVLEdBQVcsRUFBRTtRQUN2QixTQUFJLEdBQVMsSUFBSTtRQUtiLElBQUc7WUFDQyxJQUFHLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLEVBQUM7Z0JBQ3BGLGNBQWMsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDO2dCQUN0QyxjQUFjLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDO2FBQy9DO1lBQ0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxjQUFjLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO1NBQ25HO1FBQUEsT0FBTSxFQUFFLEVBQUMsR0FBRTtRQUNaLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxTQUFTLENBQUMsR0FBRyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBR0QsVUFBVSxDQUFDLGFBQWtCO1FBQ3pCLE9BQU8sSUFBSSxPQUFPLENBQU8sT0FBTyxDQUFDLEVBQUU7WUFDL0IsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsRUFBRSxDQUFDLEVBQUU7Z0JBQzNCLE9BQU8sQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQztnQkFDekIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLFdBQUksQ0FBQyxFQUFFLEVBQUUsYUFBYSxFQUFFLElBQUksQ0FBQztnQkFDN0MsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDdEIsQ0FBQyxDQUFDO1lBQ0YsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsYUFBYSxDQUFDO1FBQzNDLENBQUMsQ0FBQztJQUNOLENBQUM7SUFFRCxRQUFRLENBQUMsRUFBRTtRQUNQLE9BQU8sSUFBSSxPQUFPLENBQXVDLE9BQU8sQ0FBQyxFQUFFO1lBQy9ELE1BQU0sVUFBVSxHQUFHLENBQUMsUUFBUSxFQUFFLFdBQVcsRUFBRSxFQUFFO2dCQUN6QyxPQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUM7Z0JBQy9CLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxXQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsRUFBRSxRQUFRLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQztnQkFDL0QsT0FBTyxDQUFDLEVBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBQyxDQUFDO1lBQ3hELENBQUM7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsRUFBRSxHQUFHLFVBQVUsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLEVBQUMsQ0FBQyxDQUFDO1lBQzdELElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLEVBQUUsR0FBRyxVQUFVLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFDLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLEtBQUssQ0FBQyxFQUFFLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQywyQkFBMkIsR0FBRyxFQUFFLEVBQUUsS0FBSyxDQUFDLEVBQUMsQ0FBQyxDQUFDO1lBRWpHLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNuQixFQUFFLEVBQUUsRUFBRTtnQkFDTixJQUFJLEVBQUUsRUFBRTthQUNYLENBQUM7UUFDTixDQUFDLENBQUM7SUFDTixDQUFDO0lBRUQsUUFBUTtRQUNKLE9BQU8sSUFBSSxPQUFPLENBQWEsT0FBTyxDQUFDLEVBQUU7WUFDckMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQztZQUNsQyxDQUFDLENBQUM7WUFDRixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUMxQixDQUFDLENBQUM7SUFDTixDQUFDO0lBRUQsV0FBVztRQUNQLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxFQUFFO1lBQ3RCLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQ3pCLENBQUM7UUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUMsRUFBRTtZQUN6QixNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDOUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUMsVUFBVTtZQUNwQyxJQUFJLElBQVM7WUFDYixJQUFHO2dCQUNDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7YUFDbEM7WUFBQSxPQUFNLENBQUMsRUFBQztnQkFDTCxJQUFJLEdBQUcsT0FBTyxDQUFDLElBQUk7YUFDdEI7WUFDRCxJQUFHLE9BQU8sQ0FBQyxPQUFPLEtBQUssS0FBSyxFQUFDO2dCQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDO2FBQ2pDO2lCQUFLLElBQUcsT0FBTyxDQUFDLE9BQU8sS0FBSyxNQUFNLEVBQUM7Z0JBQ2hDLElBQUcsSUFBSSxDQUFDLElBQUksRUFBQztvQkFDYixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQztpQkFDbEM7YUFDSjtRQUNMLENBQUM7UUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsRUFBRTtZQUN4QixPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztRQUN0QixDQUFDO1FBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsQ0FBQyxFQUFFLEVBQUUsRUFBRTtZQUN6QixPQUFPLENBQUMsS0FBSyxDQUFDLG1CQUFtQixFQUFFLEVBQUUsQ0FBQztRQUMxQyxDQUFDO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBRTtZQUM1QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztRQUMxQixDQUFDLENBQUM7UUFDRixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxTQUFTLENBQUMsRUFBRTtZQUNoQyw2QkFBNkI7WUFDN0IsY0FBYyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsU0FBUyxDQUFDO1lBQzlDLGNBQWMsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsR0FBRyxJQUFJLEdBQUMsRUFBRSxHQUFDLEVBQUUsR0FBQyxFQUFFLEdBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDekcsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTO1lBQzNCLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO1FBQzNCLENBQUMsQ0FBQztRQUNGLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxFQUFFO1lBQ3JCLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQ3hCLENBQUMsQ0FBQztJQUNOLENBQUM7SUFHRCxJQUFJLENBQUMsS0FBYSxFQUFFLE9BQVksSUFBSSxFQUFFLFVBQWtCLEtBQUs7UUFDekQsSUFBSSxHQUFHLEdBQUcsSUFBSTtRQUNkLElBQUcsSUFBSSxZQUFZLE1BQU07WUFDckIsR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO1FBQzlCLE9BQU8sQ0FBQyxLQUFLLENBQUMsZUFBZSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUM7UUFFdkMscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDaEMsS0FBSyxFQUFFLEtBQUs7WUFDWixVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVU7WUFDM0IsT0FBTyxFQUFFLE9BQU87WUFDaEIsSUFBSSxFQUFFLEdBQUc7U0FDWixDQUFDLENBQUM7SUFDUCxDQUFDO0NBRUo7QUFySEQsZ0NBcUhDOzs7Ozs7Ozs7Ozs7Ozs7QUN6SEQsc0dBQW9EO0FBR3BELE1BQWEsSUFBSyxTQUFRLDJCQUFZO0lBRWxDLFlBQ1csRUFBRSxFQUNGLGFBQWEsRUFDYixNQUFNO1FBRWIsS0FBSyxFQUFFO1FBSkEsT0FBRSxHQUFGLEVBQUU7UUFDRixrQkFBYSxHQUFiLGFBQWE7UUFDYixXQUFNLEdBQU4sTUFBTTtJQUdqQixDQUFDO0lBR0QsSUFBSSxDQUFDLEtBQWEsRUFBRSxJQUFTO1FBQ3pCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsT0FBTyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUM7SUFDcEQsQ0FBQztDQUVKO0FBZkQsb0JBZUM7Ozs7Ozs7Ozs7Ozs7OztBQ2xCRCxNQUFhLFlBQVk7SUFBekI7UUFFRSxrQkFBYSxHQUFHLEVBQUU7SUEyQ3BCLENBQUM7SUF6Q0MsRUFBRSxDQUFDLEtBQWEsRUFBRSxJQUF3QztRQUN4RCxJQUFHLENBQUMsS0FBSyxJQUFJLENBQUMsSUFBSTtZQUFFLE9BQU07UUFDMUIsSUFBRyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEVBQUM7WUFDNUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFO1NBQy9CO1FBQ0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ3RDLENBQUM7SUFFRCxJQUFJLENBQUMsS0FBYSxFQUFFLFFBQTZCO1FBQy9DLElBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxHQUFHLEtBQUssRUFBRSxRQUFRLENBQUM7SUFDckMsQ0FBQztJQUVELElBQUksQ0FBQyxLQUFhLEVBQUUsSUFBUztRQUMzQixJQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEVBQUM7WUFDM0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDdEQ7UUFDRCxJQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLEVBQUM7WUFDekIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQzNEO1FBQ0QsSUFBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsRUFBQztZQUN0QyxNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7WUFDbEQsS0FBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUM7Z0JBQ25DLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ2QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDbEQ7U0FDRjtJQUNILENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxLQUFhLEVBQUUsSUFBSTtRQUNwQyxJQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEVBQUM7WUFDM0IsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDO1lBQ2hFLElBQUcsS0FBSyxJQUFJLENBQUMsRUFBQztnQkFDWixJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO2FBQzNDO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsbUJBQW1CLENBQUMsS0FBYTtRQUMvQixJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUU7SUFDaEMsQ0FBQztDQUVGO0FBN0NELG9DQTZDQzs7Ozs7Ozs7Ozs7Ozs7O0FDN0NELGtGQUF5QztBQUFoQyw0Q0FBVTtBQUNuQixnRUFBNkI7QUFBcEIsMEJBQUk7QUFDYixvR0FBbUQ7QUFBMUMsa0RBQVkiLCJmaWxlIjoiZ2FtZXNkay5qcyIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiB3ZWJwYWNrVW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbihyb290LCBmYWN0b3J5KSB7XG5cdGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlID09PSAnb2JqZWN0Jylcblx0XHRtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkoKTtcblx0ZWxzZSBpZih0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQpXG5cdFx0ZGVmaW5lKFtdLCBmYWN0b3J5KTtcblx0ZWxzZSBpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcpXG5cdFx0ZXhwb3J0c1tcImdhbWVzZGtcIl0gPSBmYWN0b3J5KCk7XG5cdGVsc2Vcblx0XHRyb290W1wiZ2FtZXNka1wiXSA9IGZhY3RvcnkoKTtcbn0pKHRoaXMsIGZ1bmN0aW9uKCkge1xucmV0dXJuICIsIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2luZGV4LnRzXCIpO1xuIiwiLy8gaHR0cHM6Ly9naXRodWIuY29tL21heG9nZGVuL3dlYnNvY2tldC1zdHJlYW0vYmxvYi80OGRjM2RkZjk0M2U1YWRhNjY4YzMxY2NkOTRlOTE4NmYwMmZhZmJkL3dzLWZhbGxiYWNrLmpzXG5cbnZhciB3cyA9IG51bGxcblxuaWYgKHR5cGVvZiBXZWJTb2NrZXQgIT09ICd1bmRlZmluZWQnKSB7XG4gIHdzID0gV2ViU29ja2V0XG59IGVsc2UgaWYgKHR5cGVvZiBNb3pXZWJTb2NrZXQgIT09ICd1bmRlZmluZWQnKSB7XG4gIHdzID0gTW96V2ViU29ja2V0XG59IGVsc2UgaWYgKHR5cGVvZiBnbG9iYWwgIT09ICd1bmRlZmluZWQnKSB7XG4gIHdzID0gZ2xvYmFsLldlYlNvY2tldCB8fCBnbG9iYWwuTW96V2ViU29ja2V0XG59IGVsc2UgaWYgKHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnKSB7XG4gIHdzID0gd2luZG93LldlYlNvY2tldCB8fCB3aW5kb3cuTW96V2ViU29ja2V0XG59IGVsc2UgaWYgKHR5cGVvZiBzZWxmICE9PSAndW5kZWZpbmVkJykge1xuICB3cyA9IHNlbGYuV2ViU29ja2V0IHx8IHNlbGYuTW96V2ViU29ja2V0XG59XG5cbm1vZHVsZS5leHBvcnRzID0gd3NcbiIsInZhciBnO1xuXG4vLyBUaGlzIHdvcmtzIGluIG5vbi1zdHJpY3QgbW9kZVxuZyA9IChmdW5jdGlvbigpIHtcblx0cmV0dXJuIHRoaXM7XG59KSgpO1xuXG50cnkge1xuXHQvLyBUaGlzIHdvcmtzIGlmIGV2YWwgaXMgYWxsb3dlZCAoc2VlIENTUClcblx0ZyA9IGcgfHwgbmV3IEZ1bmN0aW9uKFwicmV0dXJuIHRoaXNcIikoKTtcbn0gY2F0Y2ggKGUpIHtcblx0Ly8gVGhpcyB3b3JrcyBpZiB0aGUgd2luZG93IHJlZmVyZW5jZSBpcyBhdmFpbGFibGVcblx0aWYgKHR5cGVvZiB3aW5kb3cgPT09IFwib2JqZWN0XCIpIGcgPSB3aW5kb3c7XG59XG5cbi8vIGcgY2FuIHN0aWxsIGJlIHVuZGVmaW5lZCwgYnV0IG5vdGhpbmcgdG8gZG8gYWJvdXQgaXQuLi5cbi8vIFdlIHJldHVybiB1bmRlZmluZWQsIGluc3RlYWQgb2Ygbm90aGluZyBoZXJlLCBzbyBpdCdzXG4vLyBlYXNpZXIgdG8gaGFuZGxlIHRoaXMgY2FzZS4gaWYoIWdsb2JhbCkgeyAuLi59XG5cbm1vZHVsZS5leHBvcnRzID0gZztcbiIsImltcG9ydCB7IEV2ZW50RW1pdHRlciB9IGZyb20gXCIuL2V2ZW50L0V2ZW50RW1pdHRlclwiO1xyXG5pbXBvcnQgeyBSb29tIH0gZnJvbSBcIi4vUm9vbVwiO1xyXG5pbXBvcnQgV2ViU29ja2V0ID0gcmVxdWlyZShcImlzb21vcnBoaWMtd3NcIilcclxuIFxyXG5leHBvcnQgY2xhc3MgR2FtZUNsaWVudCBleHRlbmRzIEV2ZW50RW1pdHRlcntcclxuXHJcbiAgICBzZXNzaW9uX2lkOiBTdHJpbmcgPSBcIlwiXHJcbiAgICByb29tOiBSb29tID0gbnVsbFxyXG4gICAgc29ja2V0OiBXZWJTb2NrZXRcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgdXJsOiBzdHJpbmcsIHB1YmxpYyBvbkNvbm5lY3RlZDogKHJlY29ubmVjdGVkOiBib29sZWFuKSA9PiB2b2lkKXtcclxuICAgICAgICBzdXBlcigpXHJcbiAgICAgICAgdHJ5e1xyXG4gICAgICAgICAgICBpZihuZXcgRGF0ZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFwic2Vzc2lvbklkRXhwaXJlXCIpKS5nZXRUaW1lKCkgPCBuZXcgRGF0ZSgpLmdldFRpbWUoKSl7XHJcbiAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5yZW1vdmVJdGVtKFwic2Vzc2lvbklkXCIpXHJcbiAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5yZW1vdmVJdGVtKFwic2Vzc2lvbklkRXhwaXJlXCIpXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5zZXNzaW9uX2lkID0gc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShcInNlc3Npb25JZFwiKSA/IHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oXCJzZXNzaW9uSWRcIikgOiBcIlwiXHJcbiAgICAgICAgfWNhdGNoKGV2KXt9XHJcbiAgICAgICAgdGhpcy5zb2NrZXQgPSBuZXcgV2ViU29ja2V0KHVybClcclxuICAgICAgICB0aGlzLnNldHVwU29ja2V0KCk7XHJcbiAgICB9IFxyXG4gXHJcbiAgICBcclxuICAgIGNyZWF0ZVJvb20oY29uZmlndXJhdGlvbjogYW55KTogUHJvbWlzZTxSb29tPntcclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2U8Um9vbT4ocmVzb2x2ZSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMub25jZShcInJvb20gY3JlYXRlZFwiLCBpZCA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmRlYnVnKFwiaWQ6IFwiLCBpZClcclxuICAgICAgICAgICAgICAgIHRoaXMucm9vbSA9IG5ldyBSb29tKGlkLCBjb25maWd1cmF0aW9uLCB0aGlzKVxyXG4gICAgICAgICAgICAgICAgcmVzb2x2ZSh0aGlzLnJvb20pXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIHRoaXMuc2VuZChcInJvb20gY3JlYXRlXCIsIGNvbmZpZ3VyYXRpb24pXHJcbiAgICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICBqb2luUm9vbShpZCk6IFByb21pc2U8eyByb29tOiBSb29tLCByZWNvbm5lY3RlZDogYm9vbGVhbiB9PntcclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2U8eyByb29tOiBSb29tLCByZWNvbm5lY3RlZDogYm9vbGVhbiB9PihyZXNvbHZlID0+IHtcclxuICAgICAgICAgICAgY29uc3QgY3JlYXRlUm9vbSA9IChyb29tRGF0YSwgcmVjb25uZWN0ZWQpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZGVidWcoXCJkYXRhXCIsIHJvb21EYXRhKVxyXG4gICAgICAgICAgICAgICAgdGhpcy5yb29tID0gbmV3IFJvb20ocm9vbURhdGEuaWQsIHJvb21EYXRhLmNvbmZpZ3VyYXRpb24sIHRoaXMpXHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKHtyb29tOiB0aGlzLnJvb20sIHJlY29ubmVjdGVkOiByZWNvbm5lY3RlZH0pXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5vbmNlKFwicm9vbSBqb2luZWRcIiwgZGF0YSA9PiB7IGNyZWF0ZVJvb20oZGF0YSwgZmFsc2UpIH0pXHJcbiAgICAgICAgICAgIHRoaXMub25jZShcInJvb20gcmVjb25uZWN0XCIsIGRhdGEgPT4geyBjcmVhdGVSb29tKGRhdGEsIHRydWUpIH0pXHJcbiAgICAgICAgICAgIHRoaXMub25jZShcInJvb20gam9pbiBlcnJvclwiLCBlcnJvciA9PiB7IGNvbnNvbGUuZXJyb3IoXCJFcnJvciB3aGlsZSBqb2luaW5nIHJvb20gXCIgKyBpZCwgZXJyb3IpIH0pXHJcblxyXG4gICAgICAgICAgICB0aGlzLnNlbmQoXCJyb29tIGpvaW5cIiwge1xyXG4gICAgICAgICAgICAgICAgaWQ6IGlkLFxyXG4gICAgICAgICAgICAgICAgZGF0YToge31cclxuICAgICAgICAgICAgfSlcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIGdldFJvb21zKCk6IFByb21pc2U8Um9vbU1ldGFbXT57XHJcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlPFJvb21NZXRhW10+KHJlc29sdmUgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm9uY2UoXCJyb29tIGxpc3RcIiwgcm9vbXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJSb29vb29tc1wiLCByb29tcylcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgdGhpcy5zZW5kKFwicm9vbSBsaXN0XCIpXHJcbiAgICAgICAgfSlcclxuICAgIH1cclxuICAgIFxyXG4gICAgc2V0dXBTb2NrZXQoKXtcclxuICAgICAgICB0aGlzLnNvY2tldC5vbm9wZW4gPSBldiA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VuZChcInJlZ2lzdGVyXCIpXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc29ja2V0Lm9ubWVzc2FnZSA9IGV2ID0+IHtcclxuICAgICAgICAgICAgY29uc3QgbWVzc2FnZSA9IEpTT04ucGFyc2UoZXYuZGF0YS50b1N0cmluZygpKVxyXG4gICAgICAgICAgICB0aGlzLnNlc3Npb25faWQgPSBtZXNzYWdlLnNlc3Npb25faWRcclxuICAgICAgICAgICAgbGV0IGRhdGE6IGFueVxyXG4gICAgICAgICAgICB0cnl7XHJcbiAgICAgICAgICAgICAgICBkYXRhID0gSlNPTi5wYXJzZShtZXNzYWdlLmRhdGEpXHJcbiAgICAgICAgICAgIH1jYXRjaChlKXtcclxuICAgICAgICAgICAgICAgIGRhdGEgPSBtZXNzYWdlLmRhdGFcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZihtZXNzYWdlLmNvbnRleHQgPT09IFwiQUxMXCIpe1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jYWxsKG1lc3NhZ2UuZXZlbnQsIGRhdGEpXHJcbiAgICAgICAgICAgIH1lbHNlIGlmKG1lc3NhZ2UuY29udGV4dCA9PT0gXCJST09NXCIpe1xyXG4gICAgICAgICAgICAgICAgaWYodGhpcy5yb29tKXtcclxuICAgICAgICAgICAgICAgIHRoaXMucm9vbS5jYWxsKG1lc3NhZ2UuZXZlbnQsIGRhdGEpXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zb2NrZXQub25lcnJvciA9IGVyciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyKVxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNvY2tldC5vbmNsb3NlID0gKGV2KSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZGVidWcoXCJDb25lbmN0aW9uIGNsb3NlZFwiLCBldilcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMub25jZShcInJlY29ubmVjdFwiLCAoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm9uQ29ubmVjdGVkKHRydWUpXHJcbiAgICAgICAgfSlcclxuICAgICAgICB0aGlzLm9uY2UoXCJyZWdpc3RlcmVkXCIsIHNlc3Npb25JZCA9PiB7XHJcbiAgICAgICAgICAgIC8vIGtlZXAgc2Vzc2lvbiBpZCBmb3IgMyBkYXlzXHJcbiAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oXCJzZXNzaW9uSWRcIiwgc2Vzc2lvbklkKVxyXG4gICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFwic2Vzc2lvbklkRXhwaXJlXCIsIG5ldyBEYXRlKG5ldyBEYXRlKCkuZ2V0VGltZSgpICsgMTAwMCo2MCo2MCoyNCozKS50b1VUQ1N0cmluZygpKVxyXG4gICAgICAgICAgICB0aGlzLnNlc3Npb25faWQgPSBzZXNzaW9uSWRcclxuICAgICAgICAgICAgdGhpcy5vbkNvbm5lY3RlZChmYWxzZSlcclxuICAgICAgICB9KVxyXG4gICAgICAgIHRoaXMub24oXCJlcnJvclwiLCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpXHJcbiAgICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICBcclxuICAgIHNlbmQoZXZlbnQ6IHN0cmluZywgZGF0YTogYW55ID0gbnVsbCwgY29udGV4dDogc3RyaW5nID0gXCJBTExcIil7XHJcbiAgICAgICAgbGV0IG1zZyA9IGRhdGFcclxuICAgICAgICBpZihkYXRhIGluc3RhbmNlb2YgT2JqZWN0KVxyXG4gICAgICAgICAgICBtc2cgPSBKU09OLnN0cmluZ2lmeShkYXRhKVxyXG4gICAgICAgIGNvbnNvbGUuZGVidWcoXCJzZW5kaW5nIGV2ZW50XCIsIGV2ZW50LCBkYXRhKVxyXG5cclxuICAgICAgICAgICAgLy8gVE9ETzogQ2hlY2sgaWYgc29ja2V0IGlzIGNvbm5lY3RlZFxyXG4gICAgICAgICAgICB0aGlzLnNvY2tldC5zZW5kKEpTT04uc3RyaW5naWZ5KHtcclxuICAgICAgICAgICAgZXZlbnQ6IGV2ZW50LFxyXG4gICAgICAgICAgICBzZXNzaW9uX2lkOiB0aGlzLnNlc3Npb25faWQsXHJcbiAgICAgICAgICAgIGNvbnRleHQ6IGNvbnRleHQsXHJcbiAgICAgICAgICAgIGRhdGE6IG1zZ1xyXG4gICAgICAgIH0pKVxyXG4gICAgfVxyXG4gICAgXHJcbn0iLCJpbXBvcnQgeyBFdmVudEVtaXR0ZXIgfSBmcm9tIFwiLi9ldmVudC9FdmVudEVtaXR0ZXJcIjtcclxuaW1wb3J0IHsgR2FtZUNsaWVudCB9IGZyb20gXCIuL0dhbWVDbGllbnRcIlxyXG5cclxuZXhwb3J0IGNsYXNzIFJvb20gZXh0ZW5kcyBFdmVudEVtaXR0ZXIgaW1wbGVtZW50cyBSb29tTWV0YXtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgaWQsXHJcbiAgICAgICAgcHVibGljIGNvbmZpZ3VyYXRpb24sXHJcbiAgICAgICAgcHVibGljIGNsaWVudFxyXG4gICAgKXtcclxuICAgICAgICBzdXBlcigpXHJcbiAgICB9XHJcblxyXG5cclxuICAgIHNlbmQoZXZlbnQ6IHN0cmluZywgZGF0YTogYW55KXsgXHJcbiAgICAgICAgdGhpcy5jbGllbnQuc2VuZChldmVudCwgZGF0YSwgXCJST09NOlwiICsgdGhpcy5pZClcclxuICAgIH1cclxuXHJcbn1cclxuIiwiZXhwb3J0IGNsYXNzIEV2ZW50RW1pdHRlcntcclxuICAgIFxyXG4gIGV2ZW50SGFuZGxlcnMgPSB7fVxyXG5cclxuICBvbihldmVudDogc3RyaW5nLCBmdW5jOiAoZGF0YTogYW55LCBldmVudDogc3RyaW5nKSA9PiB2b2lkKXtcclxuICAgIGlmKCFldmVudCB8fCAhZnVuYykgcmV0dXJuXHJcbiAgICBpZighdGhpcy5ldmVudEhhbmRsZXJzW2V2ZW50XSl7XHJcbiAgICAgIHRoaXMuZXZlbnRIYW5kbGVyc1tldmVudF0gPSBbXVxyXG4gICAgfVxyXG4gICAgdGhpcy5ldmVudEhhbmRsZXJzW2V2ZW50XS5wdXNoKGZ1bmMpXHJcbiAgfVxyXG5cclxuICBvbmNlKGV2ZW50OiBzdHJpbmcsIGNhbGxiYWNrOiAoZGF0YTogYW55KSA9PiB2b2lkKXtcclxuICAgIHRoaXMub24oXCIjb25jZV9cIiArIGV2ZW50LCBjYWxsYmFjaylcclxuICB9XHJcblxyXG4gIGNhbGwoZXZlbnQ6IHN0cmluZywgZGF0YTogYW55KXtcclxuICAgIGlmKHRoaXMuZXZlbnRIYW5kbGVyc1tldmVudF0pe1xyXG4gICAgICB0aGlzLmV2ZW50SGFuZGxlcnNbZXZlbnRdLmZvckVhY2goZnVuYyA9PiBmdW5jKGRhdGEpKVxyXG4gICAgfVxyXG4gICAgaWYodGhpcy5ldmVudEhhbmRsZXJzW1wiKlwiXSl7XHJcbiAgICAgIHRoaXMuZXZlbnRIYW5kbGVyc1tcIipcIl0uZm9yRWFjaChmdW5jID0+IGZ1bmMoZGF0YSwgZXZlbnQpKVxyXG4gICAgfVxyXG4gICAgaWYodGhpcy5ldmVudEhhbmRsZXJzW1wiI29uY2VfXCIgKyBldmVudF0pe1xyXG4gICAgICBjb25zdCBmdW5jcyA9IHRoaXMuZXZlbnRIYW5kbGVyc1tcIiNvbmNlX1wiICsgZXZlbnRdXHJcbiAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCBmdW5jcy5sZW5ndGg7IGkrKyl7XHJcbiAgICAgICAgZnVuY3NbaV0oZGF0YSlcclxuICAgICAgICB0aGlzLmV2ZW50SGFuZGxlcnNbXCIjb25jZV9cIiArIGV2ZW50XS5zcGxpY2UoaSwgMSlcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVtb3ZlRXZlbnRIYW5kbGVyKGV2ZW50OiBzdHJpbmcsIGZ1bmMpe1xyXG4gICAgaWYodGhpcy5ldmVudEhhbmRsZXJzW2V2ZW50XSl7XHJcbiAgICAgIGNvbnN0IGluZGV4ID0gdGhpcy5ldmVudEhhbmRsZXJzW2V2ZW50XS5pbmRleE9mKGYgPT4gZiA9PT0gZnVuYylcclxuICAgICAgaWYoaW5kZXggPj0gMCl7XHJcbiAgICAgICAgdGhpcy5ldmVudEhhbmRsZXJzW2V2ZW50XS5zcGxpY2UoaW5kZXgsIDEpXHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlbW92ZUV2ZW50SGFuZGxlcnMoZXZlbnQ6IHN0cmluZyl7XHJcbiAgICB0aGlzLmV2ZW50SGFuZGxlcnNbZXZlbnRdID0gW11cclxuICB9XHJcblxyXG59IiwiZXhwb3J0IHsgR2FtZUNsaWVudCB9IGZyb20gXCIuL0dhbWVDbGllbnRcIlxyXG5leHBvcnQgeyBSb29tIH0gZnJvbSBcIi4vUm9vbVwiXHJcbmV4cG9ydCB7IEV2ZW50RW1pdHRlciB9IGZyb20gXCIuL2V2ZW50L0V2ZW50RW1pdHRlclwiIl0sInNvdXJjZVJvb3QiOiIifQ==