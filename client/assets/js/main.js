// var client = new GameClient("1231");

var client  // Client wird erst geladen, wenn die Karte eingebunden ist
var room

var isMrX = false

function onClientConnected(reconnected){
    console.log("Reconnected?", reconnected);
    
    client.on("room join error", function(error){
        console.error("AHH! Ein Fehler", error);
        
    })
    client.joinRoom("testRoom123").then(onRoomJoined)    
}

function onRoomJoined(result){
    console.log("Room joined", result);

    const room = result.room

    
    room.on("players", function(players){
        console.log("Players", players);
        
        removePlayerFigures()

        for(var i = 0; i < players.length; i++){
            var player = players[i];
            var playerEl = document.getElementById("player"+ (i+1))
            var playerElTag = playerEl.querySelector(".playerkasten .player")
            playerElTag.innerHTML = player.name;
            
            if(player.self){
                playerEl.classList.add("self")
                
                var radlEl = document.getElementById("anzahl_radl")
                radlEl.innerHTML = player.tickets[0]

                var busEl = document.getElementById("anzahl_bus")
                busEl.innerHTML = player.tickets[1]

                var seilbahnEl = document.getElementById("anzahl_seilbahn")
                seilbahnEl.innerHTML = player.tickets[2]

                

                var bootEl = document.getElementById("anzahl_boot")
                bootEl.innerHTML = player.tickets[3]

                isMrX = player.isMrX
                var doubleEl = document.getElementById("anzahl_double")
                doubleEl.innerHTML = player.tickets[4]

                bootEl.parentElement.parentElement.style.display = isMrX ? "flex" : "none"
                doubleEl.parentElement.parentElement.style.display = isMrX ? "flex" : "none"
                
            }

            // var misterxpos = null
             if(player.isMrX){
                 playerEl.classList.add("mrx")
            //     misterxpos = player.position
             }

            var tab_anz_radlEl = document.querySelector("#player" + (i+1) + " .tab_anz_radl")
            console.log("#player" + i + " .tab_anz_radl")
            var tab_anz_busEl = document.querySelector("#player" + (i+1) + " .tab_anz_bus")
            var tab_anz_seilbahnEl = document.querySelector("#player" + (i+1) + " .tab_anz_seilbahn")
            var tab_anz_bootEl = document.querySelector("#player" + (i+1) + " .tab_anz_boot")
            var tab_anz_doubleEl = document.querySelector("#player" + (i+1) + " .tab_anz_double")

            tab_anz_radlEl.innerHTML = player.tickets[0]
            tab_anz_busEl.innerHTML = player.tickets[1]
            tab_anz_seilbahnEl.innerHTML = player.tickets[2]

//            if(!player.isMrX){
//                tab_anz_bootEl.style.display = "none"
//                tab_anz_doubleEl.style.display = "none"
//            }
            tab_anz_bootEl.innerHTML = player.tickets[3]
            tab_anz_doubleEl.innerHTML = player.tickets[4]

            

            setPlayerPosition(player)
            /*

            <span class="info">Infotext</div>
            
            <div class="button important">

                <span class="info">Info im Button</div>

            </div>

            <div class="button info">

                <span class="info">2. Info im Button</div>

            </div>
            
            
            */
            // .button.important
            // .button .info
            // button -> info
            // .button.imporant
            // .button.info
            
            // #player1 .tab_anz_radl

            
            

            /* 
            round = 
            if(players.length == 4 && round == 1){
                var position = player.position
                var startPosEl = document.querySelector("#Punkte g:nth-child(" + position + ")")
                startPosEl.classList.add("pointselected")
                setTimeout(function(){
                startPosEl.classList.remove("pointselected")
            }, 5000)
            }
            else{return}
            */
        }
    })

    room.on("start", function(){
        console.log("eeee")
        setTimeout(function(){
            var figureEls = document.querySelectorAll(".playerFigure")
            console.log(figureEls)
            for(let el of figureEls){
                el.parentElement.classList.add("pointamanfang");
            }
            setTimeout(function(){
                for(let el of figureEls){
                    el.parentElement.classList.remove("pointamanfang");
                }
            }, 2500)
        }, 500) 
    })

    room.call("start", null)

    room.on("movements", function(movements){
        console.log("movements", movements)
        if(!(typeof movements === 'object')){
            movements = [movements]
        }
        var tag
        var tag_class 

        for(var i = 0; i < movements.length; i++){ 
        
            if(movements[i] == 1){
                tag = "Radl"
                tag_class = "radl";
            }else if(movements[i] == 2){
                tag = "Bus"
                tag_class = "bus"
            }else if(movements[i] == 3){
                tag = "Seilbahn"
                tag_class = "seilbahn"
            }else if(movements[i] == 4){
                tag = "Das Boot"
                tag_class = "boot"
            }
        
            var tabEl = document.getElementById("tab" + (i+1) )
            
            tabEl.parentElement.classList.add(tag_class)
            tabEl.innerHTML = tag

        }

    })
    //room.call("movements", [3, 2, 1]) 

    
    var ticketType
    var targetNode
    
    var useDoubleTicket = false



    for(let node of document.querySelectorAll("#Punkte g")){
        node.addEventListener("click", function(event){
            var nodeold = document.querySelector(".pointselected")
            if(nodeold != null){
                nodeold.classList.remove("pointselected")
                
            }
            node.classList.add("pointselected")
            targetNode = parseInt(node.querySelector("text").innerHTML)
            sendSelected()
        })
    }


    for(let ticket of document.querySelectorAll(".card")){
        ticket.addEventListener("click", function(event){
            // <div data-text="test"> -> ticket.dataset.text
            if(ticket.dataset.type == "double"){
                useDoubleTicket = !useDoubleTicket;
                if(useDoubleTicket){
                    ticket.classList.add("selected")
                }else{
                    ticket.classList.remove("selected")
                }
            }else{
                ticketType = parseInt(ticket.dataset.type.toString())
                sendSelected()  
            }
        }) 
    
    }

    function sendSelected(){
        if(targetNode == null || ticketType == null) return;
        console.log({
            ziel: targetNode,
            ticketType: ticketType,
            doubleTicket: useDoubleTicket
        })
        room.send("player select", {
            ziel: targetNode,
            ticketType: ticketType,
            doubleTicket: useDoubleTicket
        })
        useDoubleTicket = false
        document.querySelector(".card[data-type=\"double\"]").classList.remove("selected")
        var node = document.querySelector(".pointselected")
            node.classList.remove("pointselected")
        ticketType = null
        targetNode = null
    }

    room.on("state", state => {
        console.log("State", state, isMrX)
        const textfeld = document.getElementById("textfeld")
        // if (state == "MRX_SELECT") "test" else "test2"
        textfeld.innerHTML = (state == "MRX_SELECT") ? "MrX ist am Zug" : "Die Agenten sind am Zug"
        if(state == "MRX_SELECT" && isMrX){
            Snackbar.create("MrX, du bist am Zug!")
        }else if(state == "AGENT_SELECT" && !isMrX){ 
            Snackbar.create("Agenten, ihr seid am Zug!")
        }
    })
    
    room.on("round", function(round) {
        if(document.querySelector(".table-cell .current") != null){
            document.querySelector(".table-cell .current").classList.remove("current")
        }
        var roundEl = document.getElementById("roundtag")
        var numRoundEl = document.getElementById("num" + round) 
        roundEl.innerHTML= round
        roundEl.classList.add("refresh")
        numRoundEl.classList.add("current")
        setTimeout(function(){
            roundEl.classList.remove("refresh")
        }, 1000)
    })

    room.on("error", error => {
        console.log("error", error)
        Snackbar.create(error, 2000)
    })

    room.on("end", wonMrX => {

        function sprayConfetti(){
            var confettiElement = document.getElementById('my-canvas');
            var confettiSettings = { 
                "target": confettiElement,
                "max":"120",
                "size":"2",
                "animate":true,
                "props":["circle","square","triangle","line"],
                "colors":[[165,104,246],[230,61,135],[0,199,228],[253,214,126]],
                "clock":"25",
                "rotate":true,
                "width": window.innerWidth,
                "height": window.innerHeight,
                "start_from_edge":true,
                "respawn":false   
            }
            var confetti = new ConfettiGenerator(confettiSettings);
            confetti.render();
        }

        console.log("isMrX", isMrX, "wonMrX", wonMrX)


        if(isMrX == true && wonMrX == true){
            sprayConfetti();
            Snackbar.create("Du hast gegen die Agenten gewonnen!", 10000)
        }else if(isMrX == false && wonMrX == false){
            sprayConfetti();
            Snackbar.create("Ihr habt gewonnen!")
        }else if(isMrX == true && wonMrX == false){
            Snackbar.create("Herzlichen Glückwunsch, du hast verkackt..", 10000)
        }else if(isMrX == false && wonMrX == true){
            Snackbar.create("Wow ihr habts geschafft gegen einen zu verlieren", 10000)
        }
    })
    
    room.send("players get")
}

function setPlayerPosition(player){
    var position = player.position
    if(position == undefined || position == 0) return;

    var node = document.querySelector("#Punkte g:nth-child(" + position + ")") // wenn position = n, dann hol dir den n-ten knoten (child)
    console.log(node)
    var rect = node.querySelector("rect")

    var foreignEl = node.querySelector("foreignObject")
    if(foreignEl != null){
        console.log("Der Doppelbelegungsfall mit Mrx Geist ist eingetroffen")
        foreignEl.style.width = "91"
        foreignEl.innerHTML = foreignEl.innerHTML + `
         <img src="./assets/graphics/figur_halb.png" style="height: 100%;" class="${player.color} figur_halb" />
         `
    }else{
        node.innerHTML = node.innerHTML + `
        <foreignObject class="playerFigure" x="${rect.x.baseVal.value + 3}" y="${rect.y.baseVal.value - 84}" width="55.7" height="100">
        <img src="./assets/graphics/figur.png" style="height: 100%; position: absolute;" class="${player.color}" />
        </foreignObject>
        `
    }
    
    // console.log("Misterxpos:" + misterxpos);
    // console.log("PlayerPos:" + player.position + player.name)
    // if(misterxpos != null && player.position == misterxpos){
    //     var foreignEl = document.querySelector("#Punkte g:nth-child(" + position + ")" + " foreignObject")
    //     console.log(foreignEl)
    //     console.log("Der Doppelbelegungsfall mit Mrx Geist ist eingetroffen")
    //     foreignEl.innerHTML = foreignEl.innerHTML + `
    //     <img src="assets\graphics\figur_halb.png" style="height: 100%" class="${player.color} figur_halb" />
    //     `
    // }
    
    


}

function removePlayerFigures(){
    var figureEls = document.querySelectorAll(".playerFigure")
    for(let el of figureEls){
        el.remove();
    }
}


loadMap().then(function(){
    setupMapZoom()    
    // Wird erst ausgeführt, wenn die Karte eingebunden wird
    client = new gamesdk.GameClient("ws://localhost:4567/socket", onClientConnected)
})

function setupMapZoom(){
    const container = document.getElementById("karte")
    let zoom = 1;
    let isDragging = false;
    let dragStartX = 0;
    let dragStartY = 0;
    let deltaX = 0;
    let deltaY = 0;
    
    function updateZoom(){
        container.style.transform = `scale(${zoom}) translate(${deltaX}px, ${deltaY}px)`
    }
    function updateMove(){
        // ich hab irgendwas eingegeben, und es hat funktioniert
        deltaX = clamp(deltaX, -(1 - (1/zoom)) * container.clientWidth * 0.5, (1 - (1/zoom)) * container.clientWidth * 0.5)
        deltaY = clamp(deltaY, -(1 - (1/zoom)) * container.clientHeight * 0.5, (1 - (1/zoom)) * container.clientHeight * 0.5)
    }
    window.addEventListener("wheel", ev => {
        // Das hier hernehmen, wenn die Karte als SVG eingebettet wird

        let target = ev.target
        while(target.tagName != "svg"){
            target = target.parentNode
            if(target == null) return
        }
        
        if(target == container){
            zoom += ev.deltaY * -0.05
            zoom = clamp(zoom, 1, 2)
            updateMove()
            updateZoom()
        }
    })    
    window.addEventListener("mousedown", ev => {
        isDragging = true
        dragStartX = ev.x;
        dragStartY = ev.y;
    })
    window.addEventListener("mouseup", ev => {
        isDragging = false
    })
    document.getElementById("map").addEventListener("mousemove", ev => {
        if(isDragging){
            // console.log(ev.x - dragStartX, deltaX, ev.y - dragStartY, deltaY);
            
            deltaX += (ev.x - dragStartX) * (1/zoom)
            deltaY += (ev.y - dragStartY) * (1/zoom)
            
            updateMove()

            dragStartX = ev.x
            dragStartY = ev.y

            updateZoom()
        }
    })
}

function clamp(value, min, max){
    if(value < min) return min
    if(value > max) return max
    return value
}

function loadMap(){
    return new Promise(resolve => {
        var req = new XMLHttpRequest("./assets/graphics/map_background.svg")
        req.addEventListener("load", function(){
            document.getElementById("map").innerHTML = this.responseText
            resolve()
        })
        req.open("GET", "./assets/graphics/map_background.svg")
        req.send()
    })
}