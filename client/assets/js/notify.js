export class NotifyClient{

    onNotification

    constructor(roomId){
        this.roomId = roomId
        this.socket = new WebSocket("ws://localhost:4567/notify");
        this.socket.onopen = function(event){
            this.send(JSON.stringify({
                event: "connect gameroom",
                data: roomId
            }))
        }
    }

}