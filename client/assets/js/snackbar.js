

const Snackbar = {
    create: function(text, duration){
        if(!duration || duration == 0) duration = 3000
        const container = document.getElementById("snackbars")
        container.innerHTML += `
            <div class="snackbar">
                <span>${text}</span>
            </div>
        `
    

        container.classList.add("active")
        setTimeout(() => {
            const snackbar = container.querySelector(".snackbar:last-child")
            snackbar.classList.add("enter")
            setTimeout(() => {
                container.classList.remove("active")
            }, 1000)
            setTimeout(() => {
                snackbar.classList.add("exit")
            }, 300 + duration)
        }, 10)
    }
}