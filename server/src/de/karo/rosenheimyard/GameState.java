package de.karo.rosenheimyard;

public enum GameState {

    LOBBY,

    MRX_SELECT,
    AGENT_SELECT,

    GAME_OVER
}
