package de.karo.rosenheimyard;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CSVParser {

    // This parser only works for symmetrical matrices with the same width and height
    public int[][] parse(File file){
        if(!file.exists()) return null;


        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            StringBuilder builder = new StringBuilder();
            String newLine = "";
            while((newLine = reader.readLine()) != null){
                builder.append(newLine + "\n");
            }
            reader.close();

            String[] lines = builder.toString().split("\n");

            int[][] data = new int[lines.length][lines.length];
            for(int i = 0; i < lines.length; i++){
                String[] elements = lines[i].split(";");
                for(int j = 0; j < elements.length; j++){
                    data[i][j] = Integer.parseInt(elements[j]);
                }
            }

            return data;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}
