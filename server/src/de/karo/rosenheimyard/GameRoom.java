package de.karo.rosenheimyard;

import de.walamana.gamesdk.event.OnEvent;
import de.walamana.gamesdk.room.Room;
import de.walamana.gamesdk.server.Connection;
import de.walamana.gamesdk.server.GameServer;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.ArrayList;

public class GameRoom extends Room<GamePlayer> {


    final int maxRunde = 24;
    int runde = 1;                  // Erste Runde == 0
    Matrix matrix = new Matrix();

    int lastKnownLocationMrX = 0;

    ArrayList<Integer> movements = new ArrayList<>();

    GameState state = GameState.LOBBY;

    List<Integer> startlocation = new ArrayList<>(Arrays.asList(11, 35, 140, 132, 152, 160, 195, 122, 117, 30, 182, 171, 175, 100, 76, 56, 68, 94));
//    List<Integer> startlocation = new ArrayList<>(Arrays.asList(129, 128, 127, 131));

    // DIESEN KONSTRUKTOR NICHT ÄNDERN!
    public GameRoom(@NotNull JSONObject configuration, @NotNull GameServer<?> server) {
        super(configuration, server);
        matrix.setMatrix(RosenheimYard.rawMapData);
    }


    @Override
    public void onJoin(GamePlayer player){
        System.out.println("Player \"" + player.getName() + "\" joined");
        if(getPlayers().size() == 1){
            player.color = "red";
        }else if(getPlayers().size() == 2){
            player.color = "blue";
        }else if(getPlayers().size() == 3){
            player.color = "green";
        }else if(getPlayers().size() == 4){
            player.color = "yellow";
            start();
        }
        broadcast(gamePlayer -> {
            onGetPlayers(gamePlayer);
            return null;
        });
    }

    @OnEvent(event = "start")
    public void start(){
        if(state != GameState.LOBBY) return;

        System.out.println("Starting game");
        ArrayList<GamePlayer> players = getPlayers();

        int x = ThreadLocalRandom.current().nextInt(0, getPlayers().size());
        players.get(x).mrx = true;
        players.get(x).tickets[3]=3;
        players.get(x).tickets[4]=2;
        for(int i=0; i<getPlayers().size(); i++) {

            players.get(i).location = startlocation.get(ThreadLocalRandom.current().nextInt(0, startlocation.size()));
            startlocation.remove(Integer.valueOf(players.get(i).location));     // Fix issue#2: Da ArrayList.remove als "removeObj" und "removeIndex" gilt, implizit als Integer

        }


        broadcast(gamePlayer -> {
            onGetPlayers(gamePlayer);
            gamePlayer.send("round", runde);
            gamePlayer.send("mrx select", null);
            gamePlayer.send("start", null);
            return null;
        });



        setState(GameState.MRX_SELECT);



    }

    @OnEvent(event = "player select")
    public void onPlayerSelect(GamePlayer player, String data){
        if(state == GameState.GAME_OVER) return;

        JSONObject obj = new JSONObject(data);
        boolean error = false;
        int ziel = obj.getInt("ziel");
        int ticketType = obj.getInt("ticketType");
        boolean doubleTicket = obj.getBoolean("doubleTicket");

//        System.out.println("Player " + player.getName() + " selected " + ziel + " with " + ticketType);
        if(doubleTicket && player.tickets[4] <= 0){
            player.send("error", "Du hast kein Doppelticket mehr!");
            return;
        }else if(state == GameState.MRX_SELECT && !player.mrx){
            player.send("error", "MrX ist am Zug");
            return;
        }else if(state == GameState.AGENT_SELECT && player.mrx){
            player.send("error", "Die Agenten sind am Zug");
            return;
        }else if(player.hasSelected) {
            player.send("error", "Du hast deine Spielfigur bereits versetzt!");
            return;
        }else{
            ArrayList<GamePlayer> players = getPlayers();
            for(int i=0; i<getPlayers().size(); i++){
                if(players.get(i).location == ziel && players.get(i) != getmrx()){
                    player.send("error", "Feld ist schon belegt");
                    return;
                }
            }

        }
//endgame
        if(state == GameState.MRX_SELECT && player.mrx && ticketType - 1 == 3 && player.tickets[3] != 0){
            if(matrix.getConnection(player.location, ziel) != 0){
                player.tickets[3] = player.tickets[3]-1;
                player.location = ziel;

            } else{
                player.send("error", "An diese Position gibt es keine Verbindung");
                return;
            }
        } else if(state != GameState.LOBBY){
            String[] connectionTypes = (matrix.getConnection(player.location, ziel) + "").split("");

            // Radl == Typ1 => index == 0 (bzw. Typ - 1)
            if(player.tickets[ticketType - 1] != 0) {
                for(String connectionType : connectionTypes){
                    error = true;
                    if(ticketType == Integer.parseInt(connectionType)){
                        player.tickets[ticketType - 1] = player.tickets[ticketType - 1] -1;
                        player.location = ziel;
                        if(!player.mrx){
                            getmrx().tickets[ticketType - 1] += 1;
                        }
                        error = false;
                        break;
                    }
                }
            } else{
                player.send("error", "Du hast keine Tickets mehr von diesem Typ");
                return;
            }

        }else{
            System.out.println("Error!!!! " + state);
            return;
        }

        if(error) {
            player.send("error", "unmöglicher Zug");
        } else {

            /* Hier eine lambda-Funktion (bisschen wie in Mathe)
                Parameter "gamePlayer" wird projiziert auf folgende Funktion/folgenden Ablauf
                Dass diese Funktion "null" zurückgeben muss, ist der Intelligenz Javas verschuldet
            */
            broadcast(gamePlayer -> {
                onGetPlayers(gamePlayer);
                return null;
            });
            if(mrxlost()){
                endGame(false);
                return;
            }

            player.hasSelected = true;
            if(doubleTicket){
                player.hasSelected = false;
                player.tickets[4] -= 1;
                runde++;
                broadcast(gamePlayer -> {
                    gamePlayer.send("round", runde);
                    return null;
                });
                if(runde == maxRunde){
                    endGame(true);
                    return;
                }
            }


            // Der Spieler ist nicht MrX und jeder Spieler hat einen Platz ausgewählt
            if(!player.mrx && hasEveryoneSelected()){
                setState(GameState.MRX_SELECT);
                int playersStuck = 0;
                for(GamePlayer p : getPlayers()){
                    p.hasSelected = false;
                    if(isPlayerStuck(p)){
                        p.hasSelected = true;
                        player.send("error", "Es gibt keine möglichen Züge mehr für dich!");
                        playersStuck++;
                    }
                }
                if(playersStuck == getPlayers().size() - 1){
                    endGame(true);
                    return;
                }
                if(runde == maxRunde){
                    endGame(true);
                    return;
                }
                runde++;
                broadcast(gamePlayer -> {
                    gamePlayer.send("round", runde);
                    gamePlayer.send("mrx select", null);
                    return null;
                });
                // Es wird erst überprüft, ob MrX verloren hat, wenn alle Agenten ihre Spielfigur verschoben haben
                System.out.println("MrX lost? " + mrxlost());
            }else if(player.mrx){

                if(player.hasSelected) setState(GameState.AGENT_SELECT);
                movements.add(ticketType);
                if(runde == 3 || runde == 8 || runde == 13 || runde == 18 || runde == 24){
                    lastKnownLocationMrX = player.location;
                }
                revealMovements();
            }

        }

    }

    public void revealMovements(){
        broadcast(player -> {
            player.send("movements", movements);
            onGetPlayers(player);
            return null;
        });
    }

    public void endGame(boolean mrxwon){
        setState(GameState.GAME_OVER);
        lastKnownLocationMrX = getmrx().location;
        broadcast(gamePlayer -> {
            gamePlayer.send("end", mrxwon);
            onGetPlayers(gamePlayer);
            return null;
        });
    }

    public boolean hasEveryoneSelected(){
        for(GamePlayer player : getPlayers()){
            if(!player.hasSelected) return false;
        }
        return true;
    }

    public boolean isPlayerStuck(GamePlayer player){
        boolean isStuck = true;
        for(int connection : matrix.verbindung[player.location - 1]){
            if(connection == 0) continue;
            String[] connectionTypes = (connection + "").split("");
            for(String connectionType : connectionTypes){
                if(player.tickets[Integer.parseInt(connectionType)] != 0){
                    isStuck = false;
                    break;
                }
            }
        }
        return isStuck;
    }

    public GamePlayer getmrx() {
        for(GamePlayer player : getPlayers()){
            if(player.mrx) return player;
        }
        return null;
    }

    public boolean mrxlost(){
        ArrayList<GamePlayer> players = getPlayers();
        for(int i=0; i<getPlayers().size(); i++){
            if(players.get(i).location == getmrx().location && players.get(i) != getmrx()){
                return true;
            }
        }
        return false;
    }

    public void setState(GameState state){
        this.state = state;

        broadcast(gamePlayer -> {
            gamePlayer.send("state", state.name());
            return null;
        });

    }


    @Override
    @OnEvent(event = "players get")
    public void onGetPlayers(@NotNull GamePlayer player) {
        JSONArray array = new JSONArray();

        for(GamePlayer p : getPlayers()){
            JSONObject obj = p.toJSON();
            if(p == player){
                obj.put("self", true);
            }
            if(p.mrx){
                // Wenn p MrX ist und ihm das aktuelle Paket gesendet wird, sende ihm die wirkliche Position, ansonsten die zuletzt bekannte
                obj.put("position", p == player ? p.location : lastKnownLocationMrX);
            }
            array.put(obj);
        }

        player.send("players", array);
        
    }

    // DO NOT TOUCH
    @NotNull
    @Override
    public GamePlayer createNewPlayer(@NotNull Connection connection) {
        return new GamePlayer((connection));
    }
}
