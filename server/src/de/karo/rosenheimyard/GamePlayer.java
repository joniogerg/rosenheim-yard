package de.karo.rosenheimyard;

import de.walamana.gamesdk.player.Player;
import de.walamana.gamesdk.server.Connection;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GamePlayer extends Player {
    boolean mrx=false;
    int location;
    int[] tickets = new int[]{10, 8, 4, 0, 0};

    String color = "red";

    boolean hasSelected = false;

    // Ihr könnt den Konstruktor im zweifelsfall auch erweitern
    public GamePlayer(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public JSONObject toJSON() {
        JSONObject obj = super.toJSON();
        obj.put("isMrX", mrx);

        JSONArray ticketsArr = new JSONArray();
        ticketsArr.put(tickets[0]);
        ticketsArr.put(tickets[1]);
        ticketsArr.put(tickets[2]);
        ticketsArr.put(tickets[3]);
        ticketsArr.put(tickets[4]);

        obj.put("tickets", ticketsArr);
        obj.put("color", color);

        if(!mrx){
            obj.put("position", location);
        }

        return obj;
    }
}
