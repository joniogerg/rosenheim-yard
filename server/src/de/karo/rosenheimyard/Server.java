package de.karo.rosenheimyard;

import de.walamana.gamesdk.room.Room;
import de.walamana.gamesdk.server.GameServer;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import io.javalin.http.staticfiles.Location;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Field;

public class Server extends GameServer<GameRoom> {

    public Server(){

        // Das ist nicht best-practice!
        // Es wird für die Entwicklung ein Testraum mit der ID "testRoom123"

        GameRoom room = createNewRoom(new JSONObject(), this);
        getRooms().add(room);
        try{
            Field idField = Room.class.getDeclaredField("id");
            idField.setAccessible(true);
            idField.set(room, "testRoom123");
        }catch(NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e){
            e.printStackTrace();
        }

        this.getApp().config.addStaticFiles(
                System.getProperty("user.dir")
                        .replace("/server", "/client/")
                        .replace("\\server", "\\client\\"),
                Location.EXTERNAL
        );
    }

    @Override
    public GameRoom createNewRoom(JSONObject obj, GameServer server){
        return new GameRoom(obj, server);
    }

}
